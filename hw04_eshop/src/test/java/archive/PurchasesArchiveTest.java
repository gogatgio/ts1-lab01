package archive;

import shop.Item;
import shop.Order;
import shop.ShoppingCart;
import shop.StandardItem;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.Test;
import static org.mockito.Mockito.*;

public class PurchasesArchiveTest {
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;

    @BeforeEach
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
    }

    @AfterEach
    public void restoreStreams() {
        System.setOut(originalOut);
    }

    @Test
    public void testMethods() {
        //ARRANGE
        Item item = new StandardItem(1, "apple", 12.5f, "food", 100);
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addItem(item);
        shop.Order order = new Order(shoppingCart, "Pavel", "Prague", 1);
        PurchasesArchive purchasesArchive = new PurchasesArchive();

        //ACT
        // test - putOrderToPurchasesArchive
        purchasesArchive.putOrderToPurchasesArchive(order);

        //ASSERT
        // test - getHowManyTimesHasBeenItemSold
        int expectedTimesHasBeenItemSold = 1;
        Assertions.assertEquals(expectedTimesHasBeenItemSold, purchasesArchive.getHowManyTimesHasBeenItemSold(item));

        //test - printItemPurchaseStatistics
        purchasesArchive.printItemPurchaseStatistics();
        String expectedMessage = "ITEM PURCHASE STATISTICS:";
        Assertions.assertTrue(outContent.toString().contains(expectedMessage));
    }

    @Test
    public void mockConstructorParameters() {
        //ARRANGE
        int entry1Id = 1;
        int entry1SoldTimes = 10;
        ItemPurchaseArchiveEntry entry1 = mock(ItemPurchaseArchiveEntry.class);
        when(entry1.getCountHowManyTimesHasBeenSold()).thenReturn(entry1SoldTimes);
        Item item = mock(Item.class);
        when(item.getID()).thenReturn(entry1Id);
        when(entry1.getRefItem()).thenReturn(item);

        HashMap<Integer, ItemPurchaseArchiveEntry> itemArchive = new HashMap<>();
        itemArchive.put(entry1Id, entry1);
        itemArchive.put(2, mock(ItemPurchaseArchiveEntry.class));

        ArrayList<Order> orderArchive = new ArrayList<>();
        orderArchive.add(mock(Order.class));
        orderArchive.add(mock(Order.class));

        //ACT
        PurchasesArchive purchasesArchive = new PurchasesArchive(itemArchive, orderArchive);

        //ASSERT
        Assertions.assertEquals(entry1SoldTimes, purchasesArchive.getHowManyTimesHasBeenItemSold(entry1.getRefItem()));
    }
}
