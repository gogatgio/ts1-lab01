package storage;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.*;
import shop.StandardItem;

public class ItemStockTest {
    @Test
    public void itemStockConstructor() {
        //ARRANGE
        StandardItem item = new StandardItem(1, "apple", 12.5f, "food", 100);

        //ACT
        ItemStock itemStock = new ItemStock(item);

        //ASSERT
        Assertions.assertEquals(item, itemStock.getItem());
        Assertions.assertEquals(0, itemStock.getCount());
    }

    @ParameterizedTest
    @CsvSource(value = {"10,5", "10,0", "0,5", "0,0"})
    public void itemStockIncreaseDecrease(int increase, int decrease) {
        //ARRANGE
        StandardItem item = new StandardItem(1, "apple", 12.5f, "food", 100);
        ItemStock itemStock = new ItemStock(item);

        //ACT
        itemStock.IncreaseItemCount(increase);
        itemStock.decreaseItemCount(decrease);

        //ASSERT
        int expectedCount = increase - decrease;
        Assertions.assertEquals(expectedCount, itemStock.getCount());
    }
}
