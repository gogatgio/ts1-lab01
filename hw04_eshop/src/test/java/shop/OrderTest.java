package shop;

import org.junit.jupiter.api.*;

public class OrderTest {
    @Test
    public void orderConstructor() {
        //ARRANGE
        ShoppingCart shoppingCart = new ShoppingCart();
        Item item = new StandardItem(1, "apple", 12.5f, "food", 100);
        shoppingCart.addItem(item);
        String customerName = "Pavel";
        String customerAddress = "Prague";
        int state = 1;

        //ACT
        Order order = new Order(shoppingCart, customerName, customerAddress, state);

        //ASSERT
        Assertions.assertEquals(1, order.getItems().size());
        Assertions.assertEquals(customerName, order.getCustomerName());
        Assertions.assertEquals(customerAddress, order.getCustomerAddress());
        Assertions.assertEquals(state, order.getState());
    }

    @Test
    public void orderConstructorDefaultState() {
        //ARRANGE
        ShoppingCart shoppingCart = new ShoppingCart();
        Item item = new StandardItem(1, "apple", 12.5f, "food", 100);
        shoppingCart.addItem(item);
        String customerName = "Pavel";
        String customerAddress = "Prague";
        int defaultState = 0;

        //ACT
        Order order = new Order(shoppingCart, customerName, customerAddress);

        //ASSERT
        Assertions.assertEquals(1, order.getItems().size());
        Assertions.assertEquals(customerName, order.getCustomerName());
        Assertions.assertEquals(customerAddress, order.getCustomerAddress());
        Assertions.assertEquals(defaultState, order.getState());
    }

    @Test
    public void orderConstructorCartNull() {
        //ARRANGE
        ShoppingCart shoppingCart = null;
        String customerName = "Pavel";
        String customerAddress = "Prague";

        //ACT
        Exception exception = Assertions.assertThrows(Exception.class, () -> new Order(shoppingCart, customerName, customerAddress));
        String actualMessage = exception.getMessage();

        //ASSERT
        String expectedMessage = "is null";
        Assertions.assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    public void orderConstructorNameAndAddressNull() {
        //ARRANGE
        ShoppingCart shoppingCart = new ShoppingCart();
        String customerName = null;
        String customerAddress = null;

        //ACT
        Order order = new Order(shoppingCart, customerName, customerAddress);

        //ASSERT
        Assertions.assertEquals(0, order.getItems().size());
        Assertions.assertNull(order.getCustomerName());
        Assertions.assertNull(order.getCustomerAddress());
    }
}
