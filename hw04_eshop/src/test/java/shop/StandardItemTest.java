package shop;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.*;

import java.util.stream.Stream;

public class StandardItemTest {

    @Test
    public void standardItemConstructor() {
        //ARRANGE
        int id = 1;
        String name = "apple";
        float price = 12.5f;
        String category = "food";
        int loyaltyPoints = 100;

        //ACT
        StandardItem standardItem = new StandardItem(id, name, price, category, loyaltyPoints);

        //ASSERT
        Assertions.assertEquals(id, standardItem.getID());
        Assertions.assertEquals(name, standardItem.getName());
        Assertions.assertEquals(price, standardItem.getPrice());
        Assertions.assertEquals(category, standardItem.getCategory());
        Assertions.assertEquals(loyaltyPoints, standardItem.getLoyaltyPoints());
    }

    @Test
    public void standardItemCopy() {
        //ARRANGE
        int id = 1;
        String name = "apple";
        float price = 12.5f;
        String category = "food";
        int loyaltyPoints = 100;

        //ACT
        StandardItem standardItem = new StandardItem(id, name, price, category, loyaltyPoints);
        StandardItem standardItemCopy = standardItem.copy();

        //ASSERT
        Assertions.assertEquals(id, standardItemCopy.getID());
        Assertions.assertEquals(name, standardItemCopy.getName());
        Assertions.assertEquals(price, standardItemCopy.getPrice());
        Assertions.assertEquals(category, standardItemCopy.getCategory());
        Assertions.assertEquals(loyaltyPoints, standardItemCopy.getLoyaltyPoints());
    }

    @ParameterizedTest
    @MethodSource(value = "similarStandardItemProvider")
    public void standardItemEquals(StandardItem item1, StandardItem item2) {
        //ARRANGE

        //ACT

        //ASSERT
        Assertions.assertEquals(item1, item2);
    }

    private static Stream<Arguments> similarStandardItemProvider() {
        StandardItem item1 = new StandardItem(1, "apple", 12.5f, "food", 100);
        StandardItem item2 = new StandardItem(1, "apple", 12.5f, "food", 100);
        return Stream.of(
                Arguments.of(item1, item2)
        );
    }

    @ParameterizedTest
    @MethodSource(value = "notSimilarStandardItemProvider")
    public void standardItemNotEquals(StandardItem item1, StandardItem item2) {
        //ARRANGE

        //ACT

        //ASSERT
        Assertions.assertNotEquals(item1, item2);
    }

    private static Stream<Arguments> notSimilarStandardItemProvider() {
        StandardItem item1 = new StandardItem(1, "apple", 12.5f, "food", 100);
        StandardItem item2 = new StandardItem(1, "big apple", 12.5f, "food", 100);
        return Stream.of(
                Arguments.of(item1, item2)
        );
    }
}
