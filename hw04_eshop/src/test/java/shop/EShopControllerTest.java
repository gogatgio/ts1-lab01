package shop;

import archive.PurchasesArchive;
import org.junit.jupiter.api.*;
import storage.NoItemInStorage;
import storage.Storage;

public class EShopControllerTest {
    @Test
    public void fullProcessTest() throws NoItemInStorage {
        // INIT EShop
        EShopController.startEShop();
        Storage storage = EShopController.getStorage();
        int[] itemCount = {10,10,4,5,10,2};
        Item[] storageItems = {
                new StandardItem(1, "Dancing Panda v.2", 5000, "GADGETS", 5),
                new StandardItem(2, "Dancing Panda v.3 with USB port", 6000, "GADGETS", 10),
                new StandardItem(3, "Screwdriver", 200, "TOOLS", 5),
                new DiscountedItem(4, "Star Wars Jedi buzzer", 500, "GADGETS", 30, "1.8.2013", "1.12.2013"),
                new DiscountedItem(5, "Angry bird cup", 300, "GADGETS", 20, "1.9.2013", "1.12.2013"),
                new DiscountedItem(6, "Soft toy Angry bird (size 40cm)", 800, "GADGETS", 10, "1.8.2013", "1.12.2013")
        };
        for (int i = 0; i < storageItems.length; i++) {
            storage.insertItems(storageItems[i], itemCount[i]);
        }

        // CART
        ShoppingCart cart = EShopController.newCart();
        String customerName = "Libuse Novakova";
        String customerAddress = "Kosmonautu 25, Praha 8";

        // test - NoItemInStorage
        Item item = storageItems[0];
        for (int i = 0; i < storage.getItemCount(item) + 1; i++) {
            cart.addItem(item);
        }
        Exception exception = Assertions.assertThrows(NoItemInStorage.class, () -> EShopController.purchaseShoppingCart(cart, customerName,customerAddress));
        String actualMessage = exception.getMessage();
        String expectedMessage = "No item in storage";
        Assertions.assertEquals(expectedMessage, actualMessage);
        // test - storage hasn't changed
        Assertions.assertEquals(itemCount[0], storage.getItemCount(item));

        // test - cart removeItem
        cart.removeItem(item.getID());
        Assertions.assertEquals(0, cart.getItemsCount());
        // test - cart remove same item -> nothing happens
        cart.removeItem(item.getID());
        Assertions.assertEquals(0, cart.getItemsCount());

        // fill cart - correctly
        for (Item i: storageItems) {
            cart.addItem(i);
        }
        Assertions.assertEquals(storageItems.length, cart.getItemsCount());

        // PURCHASE & STORAGE
        // test - storage item counts
        for (int i = 0; i < itemCount.length; i++) {
            Assertions.assertEquals(itemCount[i], storage.getItemCount(storageItems[i]));
        }
        EShopController.purchaseShoppingCart(cart, customerName,customerAddress);
        // test - storage item counts
        for (int i = 0; i < itemCount.length; i++) {
            Assertions.assertEquals(itemCount[i] - 1, storage.getItemCount(storageItems[i]));
        }

        // ARCHIVE
        // test - getHowManyTimesHasBeenItemSold
        PurchasesArchive archive = EShopController.getArchive();
        for (Item i: storageItems) {
            Assertions.assertEquals(1, archive.getHowManyTimesHasBeenItemSold(i));
        }
    }
}
