package cz.cvut.fel.ts1;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.*;

import java.util.stream.Stream;

public class CalculatorTest {
    @Test
    public void addReturnsCorrectValue() {
        //ARRANGE
        Calculator calculator = new Calculator();

        //ACT
        int sum = calculator.add(5, 16);

        //ASSERT
        Assertions.assertEquals(21, sum);
    }

    @Test
    public void subtractReturnsCorrectValue() {
        //ARRANGE
        Calculator calculator = new Calculator();

        //ACT
        int difference = calculator.subtract(5, 16);

        //ASSERT
        Assertions.assertEquals(-11, difference);
    }

    @Test
    public void multiplyReturnsCorrectValue() {
        //ARRANGE
        Calculator calculator = new Calculator();

        //ACT
        int product = calculator.multiply(5, 16);

        //ASSERT
        Assertions.assertEquals(80, product);
    }

    @Test
    public void divideReturnsCorrectValue() {
        //ARRANGE
        Calculator calculator = new Calculator();

        //ACT
        int quotient = calculator.divide(21, 5);

        //ASSERT
        Assertions.assertEquals(4, quotient);
    }

    @Test
    public void divideThrowsError() {
        //ARRANGE
        Calculator calculator = new Calculator();

        //ACT
        Exception exception = Assertions.assertThrows(Exception.class, () -> calculator.divide(5, 0));
        String actualMessage = exception.getMessage();

        // ASSERT
        String expectedMessage = "/ by zero";
        Assertions.assertEquals(expectedMessage, actualMessage);
    }

    @ParameterizedTest(name = "add({0},{1}) = {2}")
    @CsvFileSource(resources = "/input.csv", numLinesToSkip = 1)
    public void additionTest(int a, int b, int c) {
        //ARRANGE
        Calculator calculator = new Calculator();

        //ACT
        int sum = calculator.add(a, b);

        //ASSERT
        Assertions.assertEquals(c, sum);
    }

    @ParameterizedTest
    @CsvSource(value = {"2,3,5", "3,4,7"})
    public void additionTest2(int a, int b, int c) {
        //ARRANGE
        Calculator calculator = new Calculator();

        //ACT
        int sum = calculator.add(a, b);

        //ASSERT
        Assertions.assertEquals(c, sum);
    }

    @ParameterizedTest
    @MethodSource(value = "providerMethod")
    public void additionTest3(int a, int b, int c) {
        //ARRANGE
        Calculator calculator = new Calculator();

        //ACT
        int sum = calculator.add(a, b);

        //ASSERT
        Assertions.assertEquals(c, sum);
    }

    private static Stream<Arguments> providerMethod() {
        return Stream.of(
                Arguments.of(1,2,3),
                Arguments.of(3,2,5)
        );
    }

    @ParameterizedTest
    @ValueSource(ints = {2, -4, 8, 10})
    public void isEvenTest(int a) {
        //ARRANGE
        Calculator calculator = new Calculator();

        //ACT
        boolean isEven = calculator.isEven(a);

        //ASSERT
        Assertions.assertTrue(isEven);
    }
}
