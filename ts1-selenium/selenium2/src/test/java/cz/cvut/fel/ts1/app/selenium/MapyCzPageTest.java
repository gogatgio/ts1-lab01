package cz.cvut.fel.ts1.app.selenium;

import cz.cvut.fel.ts1.app.selenium.config.DriverFactory;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class MapyCzPageTest {
    @Test
    public void fromBrnoToPrague() throws IOException {
        WebDriver driver = new DriverFactory().getDriver();
        MapyCzPage page = new MapyCzPage(driver);
        page.open();
        float d_km = (float)page.getDistance("Brno","Praha")/1000;
        assertTrue(d_km < 250 && d_km > 200);
        driver.quit();
    }

    @Test
    public void TSPMapyCzTest()  {
        List<String> cities
                = new ArrayList<>(Arrays.asList("Praha","Brno","Plzeň", "Pardubice", "Berlin"));
        MapyCzTSP tsp = new MapyCzTSP(cities);
        List<String> res = tsp.solve();
        for (String city : res) {
            System.out.print(city+" ");
        }
    }
}
