package cz.cvut.fel.ts1.app.selenium;

import cz.cvut.fel.ts1.app.api.Article;
import cz.cvut.fel.ts1.app.selenium.config.DriverFactory;
import cz.cvut.fel.ts1.app.selenium.springer.ArticlePage;
import cz.cvut.fel.ts1.app.selenium.springer.SignupLoginPage;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.openqa.selenium.WebDriver;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class SpringerTest {
    private static WebDriver driver;

    @BeforeAll
    public static void getDriver() throws IOException {
        driver = new DriverFactory().getDriver();
        SignupLoginPage signupLoginPage = new SignupLoginPage(driver);
        signupLoginPage.open();
        signupLoginPage.login();
    }

    @ParameterizedTest
    @MethodSource(value = "articleParamsProvider")
    public void testGetArticle(String title, String doi, String date) throws IOException {
        // ARRANGE
        ArticlePage articlePage = new ArticlePage(driver);
        articlePage.open(doi);

        // ACT
        Article article = articlePage.getArticle();

        // ASSERT
        Assertions.assertEquals(title, article.getTitle());
        Assertions.assertEquals(doi, article.getDoi());
        Assertions.assertEquals(date, article.getDate());
    }

    private static Stream<Arguments> articleParamsProvider() {
        return Stream.of(
                Arguments.of("TOAST: Automated Testing of Object Transformers in Dynamic Software Updates", "10.1007/s11390-021-1693-1", "31 January 2022"),
                Arguments.of("Handwritten English word recognition using a deep learning based object detection architecture", "10.1007/s11042-021-11425-7", "20 September 2021"),
                Arguments.of("Intelligent automation of invoice parsing using computer vision techniques", "10.1007/s11042-022-12916-x", "04 April 2022"),
                Arguments.of("Test-Driven Feature Extraction of Web Components", "10.1007/s11390-022-0673-4", "31 March 2022")
        );
    }
}
