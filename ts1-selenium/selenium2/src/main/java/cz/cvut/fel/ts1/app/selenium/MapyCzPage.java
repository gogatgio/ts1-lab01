package cz.cvut.fel.ts1.app.selenium;

import cz.cvut.fel.ts1.app.api.Route;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.List;

public class MapyCzPage {
    private final WebDriver driver;
    private final String url = "https://en.mapy.cz/zakladni?planovani-trasy";

    public MapyCzPage(WebDriver driver) {
        this.driver = driver;
    }

    public void open() {
        driver.get(url);
    }

    public void close() {
        driver.quit();
    }

    public long getDistance(String start, String end) {
        //enter start and end
        driver.findElement(By.xpath("//input[@placeholder='Enter a start']"))
                .sendKeys(start+ Keys.TAB);
        new WebDriverWait(driver, 10)
                .until(driver -> driver.findElement(By.cssSelector(".route-item-point")));
        driver.findElement(By.xpath("//input[@placeholder='Enter an end']"))
                .sendKeys(end+ Keys.TAB);
        new WebDriverWait(driver, 10)
                .until(driver -> driver.findElement(By.cssSelector(".highligh-time")));
        //get distance
        String[] distance = driver.findElement(By.cssSelector(".highligh-time > .distance"))
                .getText().split(" ");
        //reload
        driver.findElement(By.xpath("//button[@data-log-text='stateswitch-route']")).click();

        //parse distance
        long d;
        if(distance[1].equals("km"))
            d = (long)(Float.parseFloat(distance[0]) * 1000);
        else //meters
            d = Long.parseLong(distance[0]);
        return d;
    }

    public Route getRoute(List<String> cities) {
        Route r = new Route();
        r.setAddresses(cities);

        // Enter start
        driver.findElement(By.xpath("//input[@placeholder='Enter a start']"))
                .sendKeys(cities.get(0)+ Keys.TAB);
        new WebDriverWait(driver, 10)
                .until(driver -> driver.findElement(By.cssSelector(".route-item-point")));


        for (int i = 1; i < cities.size(); i++) {
            // Enter next city
            driver.findElement(By.xpath("//input[@placeholder='Enter an end']"))
                    .sendKeys(cities.get(i)+ Keys.TAB);
            int finalI = i;
            new WebDriverWait(driver, 10)
                    .until(driver -> driver.findElements(By.cssSelector(".route-item-point")).size() == finalI +1);
            new WebDriverWait(driver, 10)
                    .until(driver -> driver.findElements(By.cssSelector(".route-item-plus button.plus")).size() == finalI +1);

            if (i == cities.size() - 1) {
                break;
            }

            new WebDriverWait(driver, 10)
                    .until(driver -> driver.findElement(By.cssSelector(".highligh-time")));
            // Add empty destination
            driver.findElement(By.cssSelector(".route-item-plus:last-child button.plus"))
                    .sendKeys(Keys.ENTER);
            new WebDriverWait(driver, 10)
                    .until(driver -> driver.findElements(By.xpath("//input[@placeholder='Enter an end']")));
        }

        // url get distance and time
        new WebDriverWait(driver, 10)
                .until(driver -> driver.findElement(By.cssSelector(".highligh-time")));
        String time = driver.findElement(By.cssSelector(".highligh-time .time")).getText();
        r.setTime(time);
        String distance = driver.findElement(By.cssSelector(".highligh-time > .distance")).getText();
        r.setDistance(distance);

        // get url
        List<WebElement> buttons = driver.findElements(By.cssSelector(".route-summary .route-actions .icon-action button"));
        buttons.get(1).sendKeys(Keys.ENTER);
        new WebDriverWait(driver, 10)
                .until(driver -> driver.findElement(By.cssSelector(".share-tab.active .inputs input")));
        WebElement input = driver.findElement(By.cssSelector(".share-tab.active .inputs input"));
        new WebDriverWait(driver, 10)
                .until(driver -> !input.getAttribute("value").equals(""));
        String url = input.getAttribute("value");
        r.setUrl(url);

        return r;
    }
}
