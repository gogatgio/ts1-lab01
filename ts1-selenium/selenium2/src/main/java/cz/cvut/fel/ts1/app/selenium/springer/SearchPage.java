package cz.cvut.fel.ts1.app.selenium.springer;

import cz.cvut.fel.ts1.app.api.Article;
import cz.cvut.fel.ts1.app.api.Route;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;

public class SearchPage {
    private final WebDriver driver;
    private final String url = "https://link.springer.com/search";

    public SearchPage(WebDriver driver) {
        this.driver = driver;
    }

    public void open() {
        driver.get(url);
    }

    public void close() {
        driver.quit();
    }

    public List<Article> getArticles() {
        List<Article> articles = new ArrayList<>();

        // Filter articles
        driver.findElement(By.xpath("//span[text()='Article']/parent::a")).sendKeys(Keys.ENTER);
        new WebDriverWait(driver, 10)
                .until(driver -> driver.findElements(By.cssSelector("main .header .facet-constraint-message a")).stream()
                        .anyMatch(e -> e.getText().contains("Article"))
                );

        for(int i = 0; i < 4; i++) {
            // OPEN ARTICLE
            driver.findElements(By.cssSelector("main #results-list li h2 a")).get(i).sendKeys(Keys.ENTER);
            new WebDriverWait(driver, 10)
                    .until(driver -> driver.findElements(By.cssSelector("main article .c-article-header time")));

            // READ ARTICLE
            ArticlePage articlePage = new ArticlePage(driver);
            articles.add(articlePage.getArticle());

            // BACK
            driver.navigate().back();
            new WebDriverWait(driver, 10)
                    .until(driver -> driver.findElements(By.cssSelector("main .header .facet-constraint-message a")).stream()
                            .anyMatch(e -> e.getText().contains("Article"))
                    );
        }

        return articles;
    }

}
