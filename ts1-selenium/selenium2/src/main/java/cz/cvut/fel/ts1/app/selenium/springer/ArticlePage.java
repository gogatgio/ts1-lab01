package cz.cvut.fel.ts1.app.selenium.springer;

import cz.cvut.fel.ts1.app.api.Article;
import cz.cvut.fel.ts1.app.api.Route;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;


public class ArticlePage {
    private final WebDriver driver;
    private final String url = "https://link.springer.com/article/";

    public ArticlePage(WebDriver driver) {
        this.driver = driver;
    }

    public void open(String doi) {
        driver.get(url + doi);
    }

    public void close() {
        driver.quit();
    }

    public Article getArticle() {
        Article article = new Article();

        article.setTitle(driver.findElement(By.cssSelector("main article .c-article-header h1.c-article-title")).getText());
        article.setDoi(driver.findElement(By.xpath("//meta[@name='DOI']")).getAttribute("content"));
        article.setDate(driver.findElement(By.cssSelector("main article .c-article-header time")).getText());

        return  article;
    }
}
