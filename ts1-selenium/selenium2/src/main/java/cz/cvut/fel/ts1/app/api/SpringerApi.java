package cz.cvut.fel.ts1.app.api;

import cz.cvut.fel.ts1.app.selenium.MapyCzPage;
import cz.cvut.fel.ts1.app.selenium.MapyCzTSP;
import cz.cvut.fel.ts1.app.selenium.config.DriverFactory;
import cz.cvut.fel.ts1.app.selenium.springer.AdvancedSearchPage;
import org.openqa.selenium.WebDriver;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
public class SpringerApi {
    @GetMapping(value = "/springer", produces = MediaType.APPLICATION_JSON_VALUE)
    Articles getArticles() {
        Articles articles = new Articles();

        try {
            WebDriver driver = new DriverFactory().getDriver();
            AdvancedSearchPage page = new AdvancedSearchPage(driver);
            page.open();
            List<String> withAll = Arrays.asList("Page", "Object", "Model");
            List<String> withAtLeastOne = Arrays.asList("Selenium", "Testing");
            int startYear = 2022;
            int EndYear = 2022;
            articles.setArticles(page.getArticles(withAll, withAtLeastOne, startYear, EndYear));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return articles;
    }
}
