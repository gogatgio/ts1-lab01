package cz.cvut.fel.ts1.app.selenium;

import com.google.ortools.Loader;
import com.google.ortools.constraintsolver.*;
import cz.cvut.fel.ts1.app.api.Route;
import cz.cvut.fel.ts1.app.selenium.config.DriverFactory;
import lombok.Getter;
import org.openqa.selenium.WebDriver;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@Getter
public class MapyCzTSP {
    private static final Logger logger = Logger.getLogger(MapyCzTSP.class.getName());

    private long[][] matrix;
    private final List<String> cities;
    private MapyCzPage page;

    public MapyCzTSP(List<String> cities) {
        this.cities = cities;
        try {
            WebDriver driver = new DriverFactory().getDriver();
            this.page = new MapyCzPage(driver);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void computeMatrix() {
        int len = cities.size();
        matrix = new long[len][len];
        int k = 1;
        int total = len*(len - 1)/2;
        for(int i = 0; i < cities.size(); i++) {
            for(int j = i; j < cities.size(); j++) {
                if(i == j) {
                    matrix[i][j] = 0;
                } else {
                    //compute distance
                    logger.info("Computing "+(k++)+" from "+total);
                    long d = page.getDistance(cities.get(i),cities.get(j));
                    matrix[i][j] = d;
                    matrix[j][i] = d;
                }
            }
        }
    }

    private List<Long> getSolution(RoutingModel routing, RoutingIndexManager manager, Assignment solution) {
        // Solution cost.
        //solution.objectiveValue()
        // Inspect solution.
        List<Long> path = new ArrayList<>();
        long routeDistance = 0;
        long index = routing.start(0);
        while (!routing.isEnd(index)) {
            path.add((long)manager.indexToNode(index));
            long previousIndex = index;
            index = solution.value(routing.nextVar(index));
            routeDistance += routing.getArcCostForVehicle(previousIndex, index, 0);
        }
        return path;
    }

    //TODO: change to public Route solve() {
    public List<String> solve() {
        //compute matrix by selenium
        page.open();
        computeMatrix();

        Loader.loadNativeLibraries();
        RoutingIndexManager manager =
                new RoutingIndexManager(matrix.length, 1, 0);
        RoutingModel routing = new RoutingModel(manager);

        final int transitCallbackIndex =
                routing.registerTransitCallback((long fromIndex, long toIndex) -> {
                    // Convert from routing variable Index to user NodeIndex.
                    int fromNode = manager.indexToNode(fromIndex);
                    int toNode = manager.indexToNode(toIndex);
                    return matrix[fromNode][toNode];
                });

        routing.setArcCostEvaluatorOfAllVehicles(transitCallbackIndex);

        // Setting first solution heuristic.
        RoutingSearchParameters searchParameters =
                main.defaultRoutingSearchParameters()
                        .toBuilder()
                        .setFirstSolutionStrategy(FirstSolutionStrategy.Value.PATH_CHEAPEST_ARC)
                        .build();

        // Solve the problem.
        Assignment solution = routing.solveWithParameters(searchParameters);

        // Get solution
        List<Long> path = getSolution(routing, manager, solution);
        List<String> result = new ArrayList<>();
        for(Long city : path){
            result.add(cities.get(Math.toIntExact(city)));
        }

        //TODO: compute route
        //page.open();
        //Route route = page.getRoute(result);
        //page.close();
        //return route;

        result.add(result.get(0));
        return result;
    }
}
