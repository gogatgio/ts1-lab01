package cz.cvut.fel.ts1.app.api;
import lombok.*;
import java.util.List;

@Data
public class Request {
    private List<String> addresses;
}
