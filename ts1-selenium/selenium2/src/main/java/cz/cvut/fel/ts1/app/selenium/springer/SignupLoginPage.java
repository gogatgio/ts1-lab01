package cz.cvut.fel.ts1.app.selenium.springer;

import cz.cvut.fel.ts1.app.api.Route;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SignupLoginPage {
    private final WebDriver driver;
    private final String url = "https://link.springer.com/signup-login";

    public SignupLoginPage(WebDriver driver) {
        this.driver = driver;
    }

    public void open() {
        driver.get(url);
    }

    public void close() {
        driver.quit();
    }

    public void login() {
        String email = "gogatgio@fel.cvut.cz";
        String password = "YbMWRewfEY7E48W7aHMe";
        driver.findElement(By.cssSelector("form#login-box .form-field input#login-box-email")).sendKeys(email);
        driver.findElement(By.cssSelector("form#login-box .form-field input#login-box-pw")).sendKeys(password);

        driver.findElement(By.cssSelector("form#login-box .form-submit button")).sendKeys(Keys.ENTER);

        new WebDriverWait(driver, 10)
                .until(driver -> driver.findElement(By.cssSelector("body#home-page #header .cross-nav--wide button#user span")).getText().equals("Giorgi G"));
    }
}
