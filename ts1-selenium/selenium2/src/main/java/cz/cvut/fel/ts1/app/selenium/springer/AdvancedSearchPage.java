package cz.cvut.fel.ts1.app.selenium.springer;

import cz.cvut.fel.ts1.app.api.Article;
import cz.cvut.fel.ts1.app.api.Articles;
import cz.cvut.fel.ts1.app.api.Route;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;


public class AdvancedSearchPage {
    private final WebDriver driver;
    private final String url = "https://link.springer.com/advanced-search";

    public AdvancedSearchPage(WebDriver driver) {
        this.driver = driver;
    }

    public void open() {
        driver.get(url);
    }

    public void close() {
        driver.quit();
    }

    public List<Article> getArticles(List<String> withAll, List<String> withAtLeastOne, int startYear, int endYear) {
        List<Article> articles = new ArrayList<>();

        // Inputs
        driver.findElement(By.cssSelector(".form-section .form-row input#all-words"))
                .sendKeys(String.join(" ", withAll));
        driver.findElement(By.cssSelector(".form-section .form-row input#least-words"))
                .sendKeys(String.join(" ", withAtLeastOne));
        driver.findElement(By.cssSelector(".form-section .form-row input#facet-start-year"))
                .sendKeys(String.valueOf(startYear));
        driver.findElement(By.cssSelector(".form-section .form-row input#facet-end-year"))
                .sendKeys(String.valueOf(endYear));

        // Search
        driver.findElement(By.cssSelector(".form-submit-section button#submit-advanced-search"))
                .sendKeys(Keys.ENTER);
        new WebDriverWait(driver, 10)
                .until(driver -> driver.findElement(By.xpath("//span[text()='Article']/parent::a")));

        SearchPage searchPage = new SearchPage(driver);
        return searchPage.getArticles();
    }
}
