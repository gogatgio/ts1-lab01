package cz.cvut.fel.ts1.app.api;
import lombok.Data;

import java.util.List;

@Data
public class Articles {
    private List<Article> articles;
}

