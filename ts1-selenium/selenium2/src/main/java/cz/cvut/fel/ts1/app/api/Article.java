package cz.cvut.fel.ts1.app.api;
import lombok.Data;

@Data
public class Article {
    private String title;
    private String doi;
    private String date;
}

