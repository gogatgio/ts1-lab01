package cz.cvut.fel.ts1.app.api;

import cz.cvut.fel.ts1.app.selenium.MapyCzPage;
import cz.cvut.fel.ts1.app.selenium.MapyCzTSP;
import cz.cvut.fel.ts1.app.selenium.config.DriverFactory;
import org.openqa.selenium.WebDriver;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
public class MapyCzApi {

    @PostMapping(
            value = "/tsp",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    Route computeTSP(@RequestBody Request request) {
        Route r;
        //TODO: implement

        MapyCzTSP mapyCzTSP = new MapyCzTSP(request.getAddresses());
        List<String> orderedCities = mapyCzTSP.solve();
        //List<String> orderedCities = request.getAddresses();

        MapyCzPage page = mapyCzTSP.getPage();
        page.open();
        r = page.getRoute(orderedCities);

        return r;
    }
}
