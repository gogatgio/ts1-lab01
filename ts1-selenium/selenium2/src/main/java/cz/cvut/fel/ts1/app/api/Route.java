package cz.cvut.fel.ts1.app.api;
import lombok.Data;
import java.util.List;

@Data
public class Route {
    private List<String> addresses;
    private String distance;
    private String time;
    private String url;
}
